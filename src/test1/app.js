(function() {
	var input = ['0'];
	var previousValue = '0';

	var position = 0;

	var charRegex = /([-+*/])/g;

	var elements = document.getElementsByClassName('button');

	for (var i = 0; i < elements.length; i++) {
		elements[i].addEventListener('click', function() {
			var value = this.innerHTML;

			if (isChar(value)){
				if (!isChar(previousValue)) {
					previousValue = value;
					position++;
					input.push(value);
				}
			} else {
				if(input.length == 1 && input[0] == 0){
					input[0] = value;
				} else {
					if (isChar(previousValue)) {
						position++;
						input.push(value);
					}	else {
						if (value === ',') {
							if (input[position].indexOf('.') == -1){
								input[position] += '.';	
							}
						}else{
							input[position] += value;	
						}
						
					}
				}
				previousValue = value;
			}

			if(value === 'Clear'){
				position= 0;
				previousValue = '0';
				input = ['0'];
			}

			var valueToShow = '';
			for (var i = 0; i < input.length; i++) {
				if (isChar(input[i])) {
					valueToShow += ' ' + input[i] + ' ';
				} else {
					valueToShow += input[i];
				}
				
			}
			
			document.getElementById('calc-area').innerHTML = valueToShow;
		});
	}

	document.getElementById('equals').addEventListener('click', function() {
		
		while (input.indexOf('*') != -1 || input.indexOf('/') != -1) {
			var indice;
			for (var i = 0; i < input.length; i++) {
				if (isMult(input[i])) {
					indice = i;
					break;
				}
			}
			var result;
			if (input[indice] === '*') {
				result = parseFloat(input[indice - 1]) * parseFloat(input[indice + 1]);
			} else {
				result = parseFloat(input[indice - 1]) / parseFloat(input[indice + 1]);
			}		

			var temp = [];
			for (var i = 0; i < input.length; i++) {
				if (i == indice - 1 || i == indice || i == indice + 1) {
					if(i == indice) {
						temp.push(result);
					}
				}else{
					temp.push(input[i]);
				}
			}
			input = temp;
		}


		while (input.indexOf('+') != -1 || input.indexOf('-') != -1) {
			var indice;
			for (var i = 0; i < input.length; i++) {
				if (isSum(input[i])) {
					indice = i;
					break;
				}
			}
			var result;
			if (input[indice] === '+') {
				result = parseFloat(input[indice - 1]) + parseFloat(input[indice + 1]);
			} else {
				result = parseFloat(input[indice - 1]) - parseFloat(input[indice + 1]);
			}		

			var temp = [];
			for (var i = 0; i < input.length; i++) {
				if (i == indice - 1 || i == indice || i == indice + 1) {
					if(i == indice) {
						temp.push(result);
					}
				}else{
					temp.push(input[i]);
				}
			}
			input = temp;
			
		}
		document.getElementById('calc-area').innerHTML = input[0];
		position= 0;
		previousValue = '0';
		input = ['0'];
	
	});

	function isChar(val){
		return (val === '+' || val === '-' || val === '/' || val === '*');
	}

	function isSum(val){
		return (val === '+' || val === '-');
	}
	function isMult(val){
		return (val === '/' || val === '*');
	}

})();