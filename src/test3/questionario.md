## Referências Técnicas

1. Quais foram os últimos dois livros técnicos que você leu?
	- Não me lembro. Não contumo ler livros técnicos.

2. Quais foram os últimos dois framework javascript que você trabalhou?
	- AngularJS e jQuery.

3. Descreva os principais pontos positivos de seu framework favorito.
	AngularJS: 
	-O Two way data binding presente no Angular é muito bom, assim como operações como for e if para fazer estruturas como listas ajuda muito; 
	- As estruturas como Modules, services e controllers também deixam muito mais facil estruturar a aplicação.

4. Descreva os principais pontos negativos do seu framework favorito.
	AngularJS:
	- É um framework muito grande (o que não é necessariamente um ponto negativo) e por isso acredito não ser muito performatico em projetos grandes.
	- Eu acredito que a curva de aprendizagem seja maior que, por exemplo o jQuery, no entanto acredito muito mais no Angular.

5. O que é código de qualidade para você?
	- Basicamente é um código fácil para outra pessoa compreender, ou (no caso de código mantido por um equipe) código que parece ser escrito por uma só pessoa.
	- Código fácil de manter, estruturado e modular. Utilização de design patterns.

## Conhecimento de desenvolvimento

1. O que é GIT?
	- GIT é um sistema de controle de versão, um repositório para enviar o código e controlar seu crescimento e alterações. Os mais populares são Github e Bitbucket;

2. Descreva um workflow simples de trabalho utilizando GIT.

3. Como você avalia seu grau de conhecimento em Orientação a objeto?
	- De 0 à 10: 8. Já trabalhei com Java e trago essa base, no Javascript ainda tenho dificuldades, acredito, para estruturar corretamente todos os conceitos de OO.

4. O que é Dependency Injection?
	- Dependency Injection (ou injeção de dependencia) é um padrão muito utilizado nos framework MVC, em que o framework insere como um parâmetro a dependencia utilizada no controlador ou modulo evitando assim um acoplamento desnecessário entre módulos.

5. O significa a sigla S.O.L.I.D?
	- S.O.L.I.D. é im mneumônico que significa: Single responsability, Open closed, Liskov substituition, interface segregation e dependency inversion. Desenvolver em SOLID significa fazer classes ou métodos/funções com uma única responsabilidade; Deixar as entidades abertas para a extensão e fechadas para modificação; Que objetos devem ser substituíveis com instancias dos seus subtipos sem alterar o objetivo do objeto; Criar interfaces mais específicas em vez de interfaces mais gerais. Depender de abstrações, o que torna o código desacoplado.

6. O que é BDD e qual sua importância para o processo de desenvolvimento?
	-Desculpe. Nunca trabalhei com BDD.

7. Fale sobre Design Patterns.
	- Design Patterns são padrões de escrita de código, que tornam o código mais eficiente, mais facil de se manter e com menor acoplamento entre entidades.

9. Discorra sobre práticas que você considera boas no desenvolvimento javascript.
	- Considero boas algumas práticas como: manter variáveis em um escopo; utilizar EventListener em vez de onclick no html; Utilizar chaves e ponto-e-vírgula; Utilizar funções auto-executáveis e criar módulos; Utlizar "use strict"; Escrever/dar nomes para humanos entenderem.