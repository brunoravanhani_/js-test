(function() {

  var dragSrcEl = null;
  var idEscolhido;
  var props = {};


  function handleDragStart(e) {
    dragSrcEl = this;

    e.dataTransfer.effectAllowed = 'copy';
    e.dataTransfer.setData('id', this.getAttribute('id'));
  }

  function handleDragOver(e) {
    if (e.preventDefault) {
      e.preventDefault();
    }
    return false;
  }

  function handleDrop(e) {
    if (e.stopPropagation) {
      e.stopPropagation(); 
    }
    if (dragSrcEl != this) {
      setHtmlPage(e.dataTransfer.getData('id'));
    }

    return false;
  }

  function setHtmlPage(id) {
    var secao;
    var secao1 = 
      '<h1 class="title-page" id="secao1-title"></h1>'+
      '<h3 class="subtitle-page" id="secao1-subtitle"></h3>'+
      '<img class="img-page" id="secao1-image" src="">';

    var secao2 = 
      '<div class="content-page" id="secao2-content">'+
        '<h1 class="title-page" id="secao2-title"></h1>'+
        '<h3 class="subtitle-page" id="secao2-subtitle"></h3>'+
        '<a class="anchor-page" id="secao2-button"></a>'
      '</div>';

    var secao3 =
      '<h3 class="subtitle-page">Data de modificação: <span id="secao3-data"></span></h3>'+
      '<img class="img-page multiples" id="secao3-img1" src="">' +
      '<img class="img-page multiples" id="secao3-img2" src="">' +
      '<img class="img-page multiples" id="secao3-img3" src="">';

    if(id === 'secao1'){
      secao = secao1;
    }else if (id === 'secao2') {
      secao = secao2;
    } else {
      secao = secao3;
    }

    idEscolhido = id;

    document.getElementById('page-draggable').innerHTML = secao;
    props = {};
    openModal(idEscolhido);
  }

  function openModal(id){
    if (id === 'secao1') {
      openModalSecao1(id);
    } else if (id === 'secao2') {
      openModalSecao2(id);
    } else {
      openModalSecao3(id);
    }
    
  }

  function openModalSecao1(id) {
    
    if (props.id === id) {
      document.getElementById('modal-secao1-title').value    = props.title;
      document.getElementById('modal-secao1-subtitle').value = props.subtitle;
      document.getElementById('modal-secao1-image').value    = props.image;
    } else {
      document.getElementById('modal-secao1-title').value    = '';
      document.getElementById('modal-secao1-subtitle').value = '';
      document.getElementById('modal-secao1-image').value    = '';
    }
    document.getElementById('modal-secao1').classList.add('open');
    document.getElementById('modal-secao1-salvar').addEventListener('click', salvarSecao1);
  }

  function salvarSecao1() {
    
    props.id = idEscolhido;
    props.title = document.getElementById('modal-secao1-title').value;
    props.subtitle = document.getElementById('modal-secao1-subtitle').value;
    props.image = document.getElementById('modal-secao1-image').value;

    document.getElementById('secao1-title').innerHTML = props.title;
    document.getElementById('secao1-subtitle').innerHTML = props.subtitle;
    document.getElementById('secao1-image').setAttribute('src', props.image);

    document.getElementById('modal-secao1').classList.remove('open');
    
    document.getElementById('dadosJson').innerHTML = toString(props);
  }

  function openModalSecao2(id) {
    if (props.id === id) {
      document.getElementById('modal-secao2-background').value    = props.background;
      document.getElementById('modal-secao2-title').value         = props.title;
      document.getElementById('modal-secao2-subtitle').value      = props.subtitle;
      document.getElementById('modal-secao2-button-text').value   = props.buttonText;
      document.getElementById('modal-secao2-button-link').value   = props.buttonLink;
      document.getElementById('modal-secao2-button-target').value = props.buttonTarget;
    } else {
      document.getElementById('modal-secao2-background').value    = '';
      document.getElementById('modal-secao2-title').value         = '';
      document.getElementById('modal-secao2-subtitle').value      = '';
      document.getElementById('modal-secao2-button-text').value   = '';
      document.getElementById('modal-secao2-button-link').value   = '';
      document.getElementById('modal-secao2-button-target').value = '';
    }
    document.getElementById('modal-secao2').classList.add('open');
    document.getElementById('modal-secao2-salvar').addEventListener('click', salvarSecao2);
  }

  function salvarSecao2() {
    
    props.id = idEscolhido;
    props.background   = document.getElementById('modal-secao2-background').value;
    props.title        = document.getElementById('modal-secao2-title').value;
    props.subtitle     = document.getElementById('modal-secao2-subtitle').value;
    props.buttonText   = document.getElementById('modal-secao2-button-text').value;
    props.buttonLink   = document.getElementById('modal-secao2-button-link').value;
    props.buttonTarget = document.getElementById('modal-secao2-button-target').value;

    document.getElementById('secao2-content').style.background = props.background;
    document.getElementById('secao2-title').innerHTML = props.title;
    document.getElementById('secao2-subtitle').innerHTML = props.subtitle;
    document.getElementById('secao2-button').innerHTML = props.buttonText;
    document.getElementById('secao2-button').setAttribute('href', props.buttonLink);
    document.getElementById('secao2-button').setAttribute('target', props.buttonTarget);

    document.getElementById('modal-secao2').classList.remove('open');

    document.getElementById('dadosJson').innerHTML = toString(props);
  }

  function openModalSecao3(id) {
    console.log(props);
    if (props.id === id) {
      document.getElementById('modal-secao3-data').value = props.data;
      document.getElementById('modal-secao3-img1').value = props.img1;
      document.getElementById('modal-secao3-img2').value = props.img2;
      document.getElementById('modal-secao3-img3').value = props.img3;
    } else {
      document.getElementById('modal-secao3-data').value = '';
      document.getElementById('modal-secao3-img1').value = '';
      document.getElementById('modal-secao3-img2').value = '';
      document.getElementById('modal-secao3-img3').value = '';
    }
    document.getElementById('modal-secao3').classList.add('open');
    document.getElementById('modal-secao3-salvar').addEventListener('click', salvarSecao3);
  }

  function salvarSecao3() {
    
    props.id = idEscolhido;
    props.data = document.getElementById('modal-secao3-data').value
    props.img1 = document.getElementById('modal-secao3-img1').value;
    props.img2 = document.getElementById('modal-secao3-img2').value;
    props.img3 = document.getElementById('modal-secao3-img3').value;

    document.getElementById('secao3-data').innerHTML = new Date(props.data).toLocaleDateString()
    document.getElementById('secao3-img1').setAttribute('src', props.img1);
    document.getElementById('secao3-img2').setAttribute('src', props.img2);
    document.getElementById('secao3-img3').setAttribute('src', props.img3);

    document.getElementById('modal-secao3').classList.remove('open');

    document.getElementById('dadosJson').innerHTML = toString(props);
  }

  function toString(obj) {
    var jsonDados = '{';
    var i = 0;
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        if (i != 0) {
          jsonDados += ',';
        }
        i++;
        jsonDados += '<br> &nbsp;&nbsp; "' + key + '": "' + obj[key] + '"';
      }
    }
    jsonDados += '<br>}';
    return jsonDados;
  }

  var cols = document.querySelectorAll('.draggable-secao');
  [].forEach.call(cols, function(col) {
    col.addEventListener('dragstart', handleDragStart, false);
    col.addEventListener('dragover', handleDragOver, false);
    col.addEventListener('drop', handleDrop, false);
  });

  document.getElementById('button-editar').addEventListener('click', function() {
    openModal(idEscolhido);
  });

})();